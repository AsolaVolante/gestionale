package Yoppi;

public class Giocatore {
	
	private String nome;
	private Mano m;
	public Giocatore(String nome,int nCarte,MazzoDiCarte mazzo)
	{
		this.nome = nome;
		m = new Mano(nCarte,mazzo);
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	public String getNome()
	{
		return nome;
	}
	public void pescaCarta(MazzoDiCarte mazzo)
	{
		if(m.getM()[2] == null) m.pescaCarta(mazzo);
	}
	public Carta giocaCarta(int carta)
	{
		Carta c = m.getM()[carta];
		m.scartaCarta(carta);
		return c;
	}
}

