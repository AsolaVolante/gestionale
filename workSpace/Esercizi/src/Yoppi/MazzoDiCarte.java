package Yoppi;

import java.util.Random;

public class MazzoDiCarte
{
    private Carta[] mazzo = new Carta[54];
    private static int cartePescate = 0;
    public MazzoDiCarte()
    {
    	int numeroCarta = 1;
    	for(int i=0;i<54;i++)
    	{
    		if(i<13)mazzo[i] = new Carta(numeroCarta,"cuori");
    		else if(i<26) mazzo[i] = new Carta(numeroCarta,"picche");
    		else if(i<39) mazzo[i] = new Carta(numeroCarta,"quadri");
    		else if(i<52) mazzo[i] = new Carta(numeroCarta,"fiori");
    		else if(i<54) mazzo[i] = new Carta(numeroCarta,"jolly");
    		numeroCarta++;
    		if(numeroCarta > 13) numeroCarta = 1;
    	}
    }
    public void visualizzaMazzo()
    {
    	for(int i=0;i<54;i++) mazzo[i].visualizzaCarta();
    }
    public void mischiaMazzo()
    {
    	float random = (float) Math.random()*50;
    	for(int i=0;i<(int)random;i++) mischiaArray();
    }
    private void mischiaArray()
    {
	    Random rnd = new Random();
	    for (int i = mazzo.length - 1; i > 0; i--)
	    {
	      int index = rnd.nextInt(i + 1);
	      // Simple swap
	      Carta a = mazzo[index];
	      mazzo[index] = mazzo[i];
	      mazzo[i] = a;
	    }
    }
    public Carta pescaCarta()
    {
    	cartePescate++;
    	return mazzo[cartePescate-1];
    	
    }
}