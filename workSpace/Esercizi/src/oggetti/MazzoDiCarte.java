package oggetti;

public class MazzoDiCarte {
	
	
	public Carta[] mazzo = new Carta[40]; 
	
	public MazzoDiCarte()
	{	
		int j=0;
		
		//Denari
		for(int i=0; i<10; i++)
		{	
			j++;
			mazzo[i] = new Carta(j, "Danari");
		}
		
		j=0;
		
		//Bastoni
		for(int i=10; i<20; i++)
		{	
			j++;
			mazzo[i] = new Carta(j, "Bastoni");
		}
		
		j=0;
		
		//Spade
		for(int i=20; i<30; i++)
		{	
			j++;
			mazzo[i] = new Carta(j, "Spade");
		}
		
		j=0;
		
		//Coppe
		for(int i=30; i<40; i++)
		{	
			j++;
			mazzo[i] = new Carta(j, "Coppe");
		}
		
	}
	
	public void visualizzaMazzo()
	{	
		System.out.println("Mazzo: ");
		
		for(int i=0; i<40; i++)
		{	
			mazzo[i].visualizzaCarta();
		}
	}
	
	public Carta pescaCarta()
	{
		System.out.println("Carta pescata: "); 
		return mazzo[(int)(Math.random()*40)];
	}

}