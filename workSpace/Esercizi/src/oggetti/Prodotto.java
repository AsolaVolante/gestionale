package oggetti;
/*
 *  Il gestore di un negozio associa a tutti i suoi Prodotti un codice 
 *  a barre univoco, una descrizione sintetica del prodotto e il suo prezzo unitario. 
 *  Realizzare una classe Prodotti con le opportune variabili d'istanza e metodi get.
 *  Aggiungere alla classe Prodotti un metodo applicaSconto che 
 *  modifica il prezzo del prodotto diminuendolo del 5%.
 */
public class Prodotto {
	
	public int codiceABarre[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	public String descrizione;
	public double prezzo;
	
	public int[] getCodiceABarre() {
		return codiceABarre;
	}
	
	public void setCodiceABarre(int[] codiceABarre) {
		for (int i = 0; i < codiceABarre.length; i++) {
			codiceABarre[i] = (int)Math.random()*10;
		}
		this.codiceABarre = codiceABarre;
	}
	
	public String getDescrizione() {
		return descrizione;
	}
	
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	public double getPrezzo() {
		return prezzo;
	}
	
	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}
	
	public double applicaSconto(){
		return (prezzo * 0.95);
	}
}

/*
 Il gestore del negozio vuole fare una distinzione tra i prodotti Alimentari e quelli Non Alimentari .
 Ai prodotti Alimentari viene infatti associata una data di scadenza (si veda la classe Data),
 mentre a quelli Non Alimentari il materiale principale di cui sono fatti. Realizzare le sottoclassi Alimentari
 e NonAlimentari estendendo opportunamente la classe Prodotti.
 */
