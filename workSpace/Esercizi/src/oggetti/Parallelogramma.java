package oggetti;

public class Parallelogramma extends Trapezio{

	public Parallelogramma(Punto A, Punto B, Punto C, Punto D) {
		super(A, B, C, D);
		// TODO Auto-generated constructor stub
	}
	
	public double area (){
		return (distanza(A,B)*altezza)/2;
	}

	public boolean controllo() {
		boolean giusto = false;
		if (lati[0] == lati[2] && lati[1] == lati[3]) giusto = true;
		return giusto;
	}
}
