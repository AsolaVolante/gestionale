package oggetti;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Data {
	private int[] data;
	public Data(int[] data) //Costruttore esplicito
	{
		setData(data);
	}
	public void setData(int [] data) //Metodo per impostare la data chiamato anche nel costruttore
	{
		if(data.length==3) this.data = data; //Controllo sulla lunghezza dell'array
	}
	public int[] getData() //Metodo per ottenere la data
	{
		return data; 
	}
	public int[] difference() //Metodo per la differenza tra la data attuale e quella immessa
	{
		GregorianCalendar gc = new GregorianCalendar();
		int giorno = gc.get(Calendar.DATE); //giorno del calendario
		int mese = gc.get(Calendar.MONTH)+1; //mese del calendario
		int anno = gc.get(Calendar.YEAR);	//anno del calendario
		int differenzaG = 0; //differenza dei giorni
		int differenzaM = 0; //differenza dei mesi
		int cont=giorno; //contatore giorni
		while(cont!=data[0])
		{
			if(cont==30)cont=0; 
			differenzaG++;
			cont++;
		}
		cont = mese;
		while(cont!=data[1])
		{
			if(cont==12)cont=0; 
			differenzaM++;
			cont++;
		}
		int[] d = {differenzaG,differenzaM,data[2]-anno};
		return d;
	}
}