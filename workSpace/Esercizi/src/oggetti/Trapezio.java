package oggetti;

public class Trapezio extends Quadrilatero {

	protected double altezza;

	public Trapezio(Punto A, Punto B, Punto C, Punto D) {
		super(A, B, C, D);
		// TODO Auto-generated constructor stub
	}

	public double getAltezza() {
		altezza = (D.getY() - A.getY()); 
		return altezza;
	}

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}
	
	public double area() {
		double BaseMag = distanza(A,B);
		double baseMin = distanza(C,D);
		return ((baseMin + BaseMag) * altezza)/2;
	}
	
	
}
