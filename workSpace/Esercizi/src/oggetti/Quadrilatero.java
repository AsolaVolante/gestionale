package oggetti;
/*
 Scrivete una gerarchia che mostri l'ereditariet� per le classi Quadrilatero,
  Trapezio, Parallelogramma, Rettangolo e Quadrato. Utilizzate Quadrilatero come
   superclasse della gerarchia. Rendete la gerarchia profonda il pi� possibile.
    Scrivete un programma di esempio che istanzia e visualizza gli oggetti di tutte queste classi.
 */
public class Quadrilatero {

	protected Punto A;
	protected Punto B;
	protected Punto C;
	protected Punto D;	
	protected double[] lati = new double[4];
	
	public Quadrilatero(Punto A, Punto B, Punto C, Punto D) {
		this.A = A;
		this.B = B;
		this.C = C;
		this.D = D;
	}

	protected double distanza(Punto puntoA, Punto puntoB){
		float distanza = (float) Math.sqrt(Math.pow((puntoA.getX() - puntoB.getX()), 2) + Math.pow((puntoA.getY() - puntoB.getY()), 2));
		return distanza;
	}
	
	public double[] getLati()
	{
	    lati[0] = distanza(A,B);
	    lati[1] = distanza(B,C);
	    lati[2] = distanza(C,D);
	    lati[3] = distanza(D,A);

	    return lati;
	}
	
	protected double perimetro(){
		return (lati[0] + lati[1] + lati[2] + lati[3]);
	}
}
