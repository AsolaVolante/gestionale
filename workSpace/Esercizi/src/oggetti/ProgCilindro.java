package oggetti;

public class ProgCilindro {

	public static void main(String argv[]){
		Cilindro cil = new Cilindro(4,10);
		System.out.println("Dimensioni del cilindro");
		System.out.println("Area di base = "+ cil.area());
		System.out.println("Volume = "+ cil.volume());
		
		cil.setRaggio(7.6);
		cil.setAltezza(23.5);
	
		System.out.println("Dimensioni del nuovo cilindro");
		System.out.println("Area di base = "+ cil.area());
		System.out.println("Volume = "+ cil.volume());
	}
}
