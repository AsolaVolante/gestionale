package oggetti;

public class Poligono {
		
	private Punto punti[] = new Punto[10];
	private int dimensione;
	private float perimetro;
	private float area;
	/**
	 * 
	 * @param dimensione numero dei punti
	 * @param punti array di Punto
	 */
	public Poligono(int dimensione, Punto punti[]){
		this.punti = punti;
		this.dimensione = dimensione;
		perimetro();
		area = area();
	}
	
	private float distanza(Punto puntoA, Punto puntoB){
		float distanza = (float) Math.sqrt(Math.pow((puntoA.getX() - puntoB.getX()), 2) + Math.pow((puntoA.getY() - puntoB.getY()), 2));
		return distanza;
	}
	
	private void perimetro() {
		perimetro = distanza(punti[0], punti[dimensione-1]);
		for (int i = 1; i < dimensione; i++) {
			perimetro += distanza(punti[i - 1], punti[i]);
		}
	}
	
	public float getPerimetro (){
		return perimetro;
	}
	
	private float area() {
		float areaX = (punti[0].getX())*(punti[dimensione-1].getY());
		for (int i = 1; i < dimensione; i++) {
			int j=i;
			areaX += (punti[j-1].getY())*(punti[i].getX());
		}
		float areaY = (punti[0].getY())*(punti[dimensione-1].getX());
		for (int i = 1; i < dimensione; i++) {
			int j=i;
			areaY += (punti[j-1].getX())*(punti[i].getY());
		}
		float area = Math.abs((areaX-areaY)/2);
		return area;
	}
	
	public float getArea () {
		return area;
	}
}