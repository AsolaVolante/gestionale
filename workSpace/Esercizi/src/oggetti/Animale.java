package oggetti;

public class Animale {
	
	static int contaAnimali = 0;
	protected String nome;

	public String getNome() {
		return nome;
	}

	public Animale(String nome) {
		this.nome = nome;
		this.contaAnimali ++;
	}
	
	public void siPresenta(){
		System.out.println("Un animale si presenta: ");
		System.out.println("Sono un animale, mi chiamo " + getNome());
	}
}
