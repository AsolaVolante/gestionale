package oggetti;

public class Rettangolo extends Parallelogramma {

	public Rettangolo(Punto A, Punto B, Punto C, Punto D) {
		super(A, B, C, D);
		// TODO Auto-generated constructor stub
	}
	public double area(){
		return distanza(A,B) * altezza;
	}
	
	public boolean controllo() {
		boolean giusto = true;
		if (lati[0] == lati[2] && lati[1] == lati[3]) giusto = false;
		return giusto;
	}
}
