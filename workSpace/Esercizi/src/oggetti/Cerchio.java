package oggetti;

public class Cerchio {
	
	private double raggio;
	
	public Cerchio(double r){
		raggio = r;
	}

	public void getRaggio(double r) {
		raggio = r;
	}

	public void setRaggio(double raggio) {
		this.raggio = raggio;
	}
	
	public double area(){
		return (raggio*raggio*Math.PI);
	}
	
	public double circonferenza(){
		return (2*raggio*Math.PI);
	}
	
}
