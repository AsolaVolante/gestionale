package oggetti;

public class Dipendente extends Anagrafica{
	
	protected int level = 1;
	protected int stipendio;
	protected int stipendioP;
	
	public void setLevel(int level){
		
		if(level > 0 && level < 8) {
			
				this.level = level;
				stipendioP += stipendio * ((this.level*10)/100);
			}
	}
	
	public int getLevel(){
		return level;
	}
	
	public void setStipendio(int stipendio)	{
		this.stipendio = stipendio;
		stipendioP += this.stipendio * ((level*10)/100);
	}
	
	public int getStipendio()	{
		return stipendioP; 
	}
}