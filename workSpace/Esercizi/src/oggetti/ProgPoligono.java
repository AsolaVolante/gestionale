package oggetti;
import java.util.Scanner;
/*
 ricreare con gli oggetti la distanza tra due punti: classe Punto,
 classe Poligono che ha attibuti un vettore di (max) 10 Punti e metodi distanza (privato),
 perimetro e area come da lezioni precedenti - caircare una classe ciascuno
 */
public class ProgPoligono {
	
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		int numeroPunti;
		
		Punto[] arrayPunti = new Punto[10];
		
		System.out.println("quanti vertici ha questa figura?");
				
		numeroPunti = in.nextInt();
		
		for (int i = 0; i < numeroPunti; i++) {
			System.out.println("Inserisci la x del punto " + (i+1));
			int x = in.nextInt();
			System.out.println("Inserisci la y del punto " + (i+1));
			int y = in.nextInt();
			
			arrayPunti[i] = new Punto(x, y);
		}

		Poligono poligono = new Poligono(numeroPunti, arrayPunti);

		System.out.println("Perimetro = " + poligono.getPerimetro());
		
	    System.out.println("Area = " + poligono.getArea());
		
		in.close();
	}
}