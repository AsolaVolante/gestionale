package oggetti;

public class Studentee extends Persona {
	
	protected String scuola;
	protected int matricola;
	
	public Studentee(String nome, String cognome, String indirizzo, int matricola, String scuola) {
		super(nome, cognome, indirizzo);
		this.matricola = matricola;
		this.scuola = scuola;
	}
	
	public void si_presenta(){
		super.si_presenta();
		System.out.println("Frequento l' istituto "+ scuola +" e la mia matricola �: "+ matricola);

	}
	 
	public void cambiocasa(String nuovoIndirizzo, String scuola){
		super.cambiocasa(nuovoIndirizzo);
		this.scuola = scuola;
	}
}
