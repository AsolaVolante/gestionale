package oggetti;
/*
 * Creare una classe ResponsabileDiProgetto che estende Dipendente (vedi esercizi gi� fatti)
 * e aggiunge l'attributo bonus che concorre alla formazione dello stipendio. Si crei anche opportunamente
 * un costruttore con parametri per il nostro responsabile di progetto che richiami il costruttore del dipendente.
 * Provare a costruire un dipendente e un responsabile e verificare che i metodi stipendio richiamati sono diversi.
 */
public class ResponsabileDiProgetto extends Dipendente {
	
	protected int bonus = 50;
	
	public ResponsabileDiProgetto(int bonus, int level, int stipendio, int stipendioP){
		this.level = level; 
		this.stipendio = stipendio + bonus;
		this.stipendioP = stipendioP;
		
	}
}
