package oggetti;

public class Frazione 
{
 //attributi mascherati
 private int numeratore=1;
 private int denominatore=1;
 
 //COSTRUTTORE
 //imposta solo valori+
 
 public Frazione(int num, int den)
 {
	 if(num>0)
	 {
		 numeratore= num;
	 }
	 
	 if(den>0)
	 {
		 denominatore=den;
	 }
 }
 
 public boolean ispropria(int num, int den)
 {
	 if(num<den)
		 return true;
	 
	 return false;
 }
 
 public boolean isimpropria(int num, int den)
 {
	 if(num>den)
		 return true;
	 
	 return false;
 }

 private int calcolaMCD(int a, int b)
 {
	 int temp,resto;
	 
	 if(a<b)
	 {
		 temp= a;
		 a= b;
		 b= temp;
	 }
	 
	 resto= a%b;
	 
	 while(resto !=0)
	 {
		 a=b;
		 b=resto;
		 resto= a%b; 
		 
		 if(resto==0)
		 {
			isapparente();
		 }
	 }
	 
	 return b;
 }
 
 public boolean isapparente()
 {
	 return true;
 }
 
 //metodo di interfaccia
 public boolean semplifica()
 {
	 int mcd= calcolaMCD(numeratore,denominatore);
	 
	 if(mcd!=1)
	 {
		 numeratore= numeratore/mcd;
		 denominatore= denominatore/mcd;
		 return true;	 
	 }
	 return false;
 }
 
 //metodo di interfaccia
 public void mostra()
 {
	 System.out.println(numeratore);
	 System.out.println("-----------");
	 System.out.println(denominatore);
 }
}