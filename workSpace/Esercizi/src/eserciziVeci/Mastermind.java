package eserciziVeci;
/*
All�inizio il computer decide 4 colori scelti tra 6(rosso,giallo,verde,azzurro,nero,bianco) in una precisa sequenza (vettore)
Il giocatore, di volta in volta, fornisce una sequenza di 4 colori, che sar� memorizzata.
Il computer da il feedback al giocatore in questo modo, stampando un vettore di 4 elementi, nel quale ogni elemento �:
X: indica che un colore � esatto ma nella posizione sbagliata
O: indica che un colore � esatto e nella posizione corretta
Il gioco finisce quando sono finiti tutti i tentativi o se il codice viene indovinato.
 */
import java.util.Scanner;

public class Mastermind {
	
	static int [] sceltaSegreta = {0, 0, 0, 0};
	static int [] sceltaUtente  = {0, 0, 0, 0};
	static int giustaNelPostoGiusto = 0;
	static int giustaNelPostoWrong  = 0;
	static int [][] storico = new int [6][10];
	
	public static void generaSequenza(){
		for(int i=0; i<sceltaSegreta.length; i++){
			sceltaSegreta[i]=(int)((Math.random()*6)+1);
		}
	}

	private static void men� (){
		
		System.out.println("########################################");
		System.out.println("#              MASTERMIND   	       #");
		System.out.println("# 1. Regole/come giocare               #");
		System.out.println("# 2. Colori disponibili                #");
		System.out.println("# 3. Inizia una partita contro la CPU  #");
//		System.out.println("# 4. Crea Tu una partita contro la CPU #");
		System.out.println("# 0. Esci dal programma                #");
		System.out.println("########################################");
		
	}
	private static void regole (){
		
		System.out.println("                    IL REGOLAMENTO DI MASTERMIND:");
		System.out.println("Dovrai affrontare la CPU indovinando una sequenza di 4 colori ");
		System.out.println("disposti in un ordine preciso. Il giocatore immetter� una sequenza");
		System.out.println("di 4 colori e li deve disporre nello stesso identico modo con cui la CPU li ha disposti.");
		System.out.println();
		System.out.println("Il gioco termina quando il giocatore indovina la sequenza di");
		System.out.println("colori e li dispone nello stesso ordine con cui li ha disposti la CPU.");
		System.out.println();
		System.out.println("RICORDA! SOLO 10 TENTATIVI \n");

	}
private static void colori (){
		System.out.println(" i colori sono: ROSSO , GIALLO, VERDE  ");
		System.out.println("                AZZURRO , NERO, BIANCO ");
		System.out.println(" RICORDA: i colori vengono presi e disposti in modo casuale e potrebbero essere doppi. \n");
	}
/**
 * converte la stringa composta da r,g,v,a,n,b in numeri da 1 a 6. ritorna 0 se non ci riesce.
 * @param colore stringa da convertire
 * @return return 1 = r, 2 = g, 3 = v, 4 = a, 5 = n, 6 = b, 0 se errore
 */
	private static int convertiColore(String colore){
		colore = colore.toUpperCase();
		char c = colore.charAt(0); // Prende il primo carattere
		
		if (c == 'R')
			return 1;
		if (c == 'G')
			return 2;
		if (c == 'V')
			return 3;
		if (c == 'A')
			return 4;
		if (c == 'N')
			return 5;
		if (c == 'B')
			return 6;
		return 0;
	}
	
	/**
	 * Inverte il colore in intero nel colore in Stringa
	 * @param colore intero da convertire
	 * @return "" se errore, stringa con colore altrimenti
	 */
	private static String inevertiColore(int colore){
		switch(colore){
		case 1: return "Rosso";
		case 2: return "Giallo";
		case 3: return "Verde";
		case 4: return "Azzurro";
		case 5: return "Nero";
		case 6: return "Bianco";
		}
		return "";
	}
	public static void stampaMatrice (int mosse){
		System.out.println("colori\t\t\t\tGG\tGS");
		for (int i = 0; i <= mosse; i++) {
			for (int j = 0; j < 6; j++) {
				if(j<4){
					System.out.print(inevertiColore(storico[j][i])+"\t");
				}
				else
					System.out.print(storico[j][i]+"\t");
			}
			System.out.println();
		}
	}
	public static void main(String[] args) {	
		
		Scanner in = new Scanner(System.in);
		int scletaUtente;
		 
		do{
			men�();
			scletaUtente = in.nextInt();
			
			switch (scletaUtente) {
			case 0: break;
			
			case 1: regole();
				break;

			case 2: colori(); 
				break;
			
			case 3: {
				// gioco
				System.out.println("INCOMINCIAMO:\n");
				System.out.println("I colori potrebbero essere doppi, essi sono: Rosso, Nero, Bianco, Verde, Giallo, Azzurro");

				generaSequenza();
				/*
				System.out.println("Io ho scelto:");
				for (int i = 0; i < sceltaSegreta.length; i++) {
					System.out.println(inevertiColore( sceltaSegreta[i] ) + "\t");
				}*/
				
				boolean indovinato = false;
				boolean conferma = false;
				for(int k = 0; k < 10 && !indovinato; k++){

					System.out.println("\nDammi la sequenza:\n");

					do{	
					for (int i = 0; i < 4; i++) {
						
						System.out.println("Iniziamo con il " + (i+1) + "� colore");
						
						int colore=0;
	
						do {
							String coloreInput;
							coloreInput=in.next();
							colore = convertiColore(coloreInput);
							if (colore == 0) {
								System.out.println("C'� stato un errore di lettura, per favore reinserisci...");
								System.out.println("I colori sono: Rosso, Nero, Bianco, Verde, Giallo, Azzurro");
							}
						}while(colore == 0);// Se c'� errore in lettura ripete la lettura
						
						// Salva il colore nell'array
						sceltaUtente[i] = colore;
					}
					System.out.println("Vuoi rifare la sequenza? s/n");
					String risposta = in.next();
					if (risposta.charAt(0) == 'n')
						conferma = true;
					else conferma= false;
					}while (conferma == false);
					
					System.out.println("Hai scelto la sequenza: ");
					for (int i = 0; i < sceltaUtente.length; i++) {
						System.out.print(inevertiColore( sceltaUtente[i] ) + " ");
					}
					
					// Azzeriamo le variabili di suggerimento
					giustaNelPostoGiusto = 0;
					giustaNelPostoWrong  = 0;
					
					for (int i = 0; i < sceltaUtente.length; i++) {
						int temp = sceltaUtente[i];
						storico[i][k] = temp;
					}
					
					// Creiamo l'array temporaneo
					int temp[] = new int[4];
					for (int i = 0; i < temp.length; i++) {
						temp[i] = sceltaSegreta[i];
					}
					
					// Controllo prima quelli giusti
					for (int i = 0; i < temp.length; i++) {
						if (temp[i] == sceltaUtente[i]){
							giustaNelPostoGiusto++;
							temp[i] = -1;
							sceltaUtente[i] = -2;
						}
					}
					// Poi quelli sbagliati
					for (int i = 0; i < temp.length; i++) {
						for (int j = 0; j < sceltaUtente.length; j++) {
							if (temp[i] == sceltaUtente[j]){
								giustaNelPostoWrong++;
								// Elimino il colore
								temp[i] = -1;
								sceltaUtente[j] = -2;
							}
						}
					}
					storico[4][k] = giustaNelPostoGiusto;
					storico[5][k] = giustaNelPostoWrong;
					
					
					if(giustaNelPostoGiusto == 4){
						indovinato = true;
						System.out.println("\nHai indovinato in " + (k+1) + " tentativi!\nLa sequenza giusta �:");
						for (int i = 0; i < sceltaSegreta.length; i++) {
							System.out.print(inevertiColore( sceltaSegreta[i] ) + "\t");
						}
					}
					else {
						System.out.println("\nHai messo:\n" + giustaNelPostoGiusto + " colori giusti nel posto giusto");
						System.out.println(giustaNelPostoWrong + " colori giusti nel posto sbagliato");
						stampaMatrice(k);
					}
				}
				
				if (indovinato) {
					System.out.println("Hai vinto, complimenti!");
				}
				else
				{
					System.out.println("Hai perso, complimenti!\t(mona)!\nLa sequenza era:");
					for (int i = 0; i < sceltaSegreta.length; i++) {
						System.out.println(inevertiColore( sceltaSegreta[i] ) + "\t");
					}
				}			
			}
				break;
				
			default:		System.out.println("#ERRORE DI IMMISIONE#");
				break;
			}
		}while (scletaUtente!=0);
		in.close();
		System.out.println("Grazie per aver usato il programma");
	}
}
