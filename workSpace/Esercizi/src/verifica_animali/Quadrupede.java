package verifica_animali;

public class Quadrupede extends Animale {
	
	protected String propulsione, movimento;
	protected int zampe;
	
	public String getNome() {
		return nome;
	}

	public String getPropulsione() {
		return propulsione;
	}

	public String getMovimento() {
		return movimento;
	}

	public int getZampe() {
		return zampe;
	}

	public Quadrupede(String nome) {
		super(nome);
		this.movimento = getMovimento();
		this.propulsione = getPropulsione();
		this.zampe = getZampe();
	}
	
	public void siPresenta() {
		System.out.println("Un quadrupede si presenta:");
		System.out.println("Ciao, sono un quadrupede e mi chiamo "+ getNome() + ", ho "+ getZampe() + " zampe e mi sposto con " + getPropulsione() +". In che modo? "+ getMovimento());
	}
}
