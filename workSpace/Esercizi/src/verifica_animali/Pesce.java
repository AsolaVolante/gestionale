package verifica_animali;

public class Pesce extends Animale {

	protected String propulsione, movimento;
	
	public String getNome() {
		return nome;
	}


	public String getPropulsione() {
		return propulsione;
	}

	public String getMovimento() {
		return movimento;
	}
	
	public Pesce(String nome) {
		super(nome);
		this.movimento = getMovimento();
		this.propulsione = getPropulsione();
	}
	
	public void siPresenta() {
		System.out.println("Un pesce si presenta:");
		System.out.println("Ciao, sono un pesce e mi chiamo "+ getNome() + ", non ho zampe, ma mi sposto con " + getPropulsione() +". In che modo? "+ getMovimento());
	}

}
