package verifica_animali;

public class Passero extends Uccello {

	protected String propulsione, movimento, verso, luogo;
	
	public String getVerso() {
		return verso;
	}

	public String getLuogo() {
		return luogo;
	}
	
	private void buonanotte() {
		System.out.println("Buonanotte: vado a dormire sul "+ luogo);
	}

	public String getPropulsione() {
		return propulsione;
	}

	public String getMovimento() {
		return movimento;
	}
	public Passero(String nome) {
		super(nome);
		this.movimento = getMovimento();
		this.propulsione = getPropulsione();
		this.verso = getVerso();
		this.luogo = getLuogo();
	}
	
	public void siPresenta() {
		System.out.println("Un Passero si presenta:");
		System.out.println("Ciao, sono un Passero e mi chiamo "+ getNome() + " e amo "+ getVerso() + ", ho 2 zampe e mi sposto con " + getPropulsione() +". In che modo? "+ getMovimento());
		buonanotte();
	}
}
