package GIACCHETTI_VERMAG25;

public class Punto {
	//OVERRIDING; tra 2 classi, di un metodo c'� lo stesso nome e stesso parametro
	//OVERLOADING;	in una classe ci sono 2 metodi con stesso nome e parametri diversi
	protected float x,y;
	/**
	 * @param x coordinata del punto
	 * @param y coordinata del punto
	 */
	public Punto(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public float getX(){
		return x;
	}

	public float getY() {
		return y;
	}

	public void setX(float x) {
		this.x = x;
	}

	public void setY(float y) {
		this.y = y;
	}
	//questo sotto � un override del java.Object.toString
	public String toString() {
		return "x = "+x+" , y = "+y;
	}
	
	public boolean equals(Punto B){
		if (this.getX() == B.getX()){
			if(this.getY() == B.getY()){
			return (true);
			}
		}
		return false;
	}
}