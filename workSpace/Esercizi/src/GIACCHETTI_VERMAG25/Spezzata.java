package GIACCHETTI_VERMAG25;


public class Spezzata{
	
	private Segmento punti[] = new Segmento[20];
	
	public Spezzata(Punto punti[]) {
		
		this.punti = punti;
		
	}
	protected float lunghezza(Punto puntoA, Punto puntoB){
		float lunghezza = (float) Math.sqrt(Math.pow((puntoA.getX() - puntoB.getX()), 2) + Math.pow((puntoA.getY() - puntoB.getY()), 2));
		return lunghezza;
	}
	private float distanza(Punto punti[]){

		float perimetro = lunghezza(punti[0], punti[punti.length-1]);
		for (int i = 1; i < punti.length; i++) {
			perimetro += lunghezza(punti[i - 1], punti[i]);
		}
		return perimetro;
	}
}
