package GIACCHETTI_VERMAG25;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);

		float x;
		float y;
		float z;
		
		int risposta;
		
		boolean esciCiclo = false;
		
		do{	
			Menu();

			risposta = in.nextInt();

			switch(risposta){
				
			case 1:
					System.out.println("Inserisci la x del punto A");
					x = in.nextInt();
					System.out.println("Inserisci la y del punto A" );
					y = in.nextInt();
					Punto A = new Punto(x,y);
					
					System.out.println("Inserisci la x del punto B");
					x = in.nextInt();
					System.out.println("Inserisci la y del punto B" );
					y = in.nextInt();
					Punto B = new Punto(x,y);
					//si presentano con toString
					System.out.println("punto A = "+A.toString());
					System.out.println("punto B = "+B.toString());			
					System.out.print(" ");		
					//equals: il primo � uguale al secondo?
					A.equals(B);
				break;
						
			case 2: 
					System.out.println("Inserisci la x del pixel");
					x = in.nextInt();
					System.out.println("Inserisci la y del pixel" );
					y = in.nextInt();
					System.out.println("color?" );
					String colore = in.next();
					Pixel Blu = new Pixel(x,y,colore);
					
					System.out.println("Inserisci la x del pixel");
					x = in.nextInt();
					System.out.println("Inserisci la y del pixel" );
					y = in.nextInt();
					System.out.println("color ?" );
					colore = in.next();
					Pixel Rosso = new Pixel(x,y,colore);
					//si presentano con toString
					System.out.println("primo pixel = "+Blu.toString());
					System.out.println("secondo pixel = "+Rosso.toString());
					System.out.print(" ");		
					//equals: il primo � uguale al secondo?
					Blu.equals(Rosso);
				break;
			
			case 3: 
					System.out.println("Inserisci la x del punto3D");
					x = in.nextInt();
					System.out.println("Inserisci la y del punto3D" );
					y = in.nextInt();
					System.out.println("Inserisci la z del punto3D" );
					z = in.nextInt();
					System.out.println("color ?" );
					colore = in.next();
					Punto3D c = new Punto3D(x,y,colore,z);
					
					System.out.println("Inserisci la x del punto3D");
					x = in.nextInt();
					System.out.println("Inserisci la y del punto3D" );
					y = in.nextInt();
					System.out.println("Inserisci la z del punto3D" );
					z = in.nextInt();
					System.out.println("color?" );
					colore = in.next();
					Punto3D v  = new Punto3D(x,y,colore,z);
					//si presentano  con toString
					System.out.println("primo punto3D = "+c.toString());
					System.out.println("secondo punto3D = "+v.toString());
					//equals: il primo � uguale al secondo?
					System.out.print(" ");		
					c.equals(v);
				break;
			
			case 4:
					System.out.println("Inserisci la x del punto A");
					x = in.nextInt();
					System.out.println("Inserisci la y del punto A" );
					y = in.nextInt();
					Punto C = new Punto(x,y);
					
					System.out.println("Inserisci la x del punto B");
					x = in.nextInt();
					System.out.println("Inserisci la y del punto B" );
					y = in.nextInt();
					Punto D = new Punto(x,y);
					
					Segmento CD = new Segmento(C,D);
					System.out.println("lunghezza = "+CD.lunghezza(C,D));
					
				break;
			case 0:
					esciCiclo = true;
				break;
			
			default:
					System.out.println("La prossima volta inserisci un numero del menu...");
				break;
			}	
		}while(!esciCiclo);
	
	in.close();
	
		
	}
			
	private static void Menu() {
		System.out.println("");
		System.out.println("scegli:");
		System.out.println("        1. 2 Punti");
		System.out.println("        2. 2 Pixel");
		System.out.println("        3. 2 Punti 3D");
		System.out.println("        4. segmento con 2 punti");
		
		System.out.println("        0. esci");
	
	}

}
