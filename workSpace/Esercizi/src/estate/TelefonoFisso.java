package estate;

 class TelefonoFisso {
	 
	 protected short ore;	
	 protected short minuti;
	 protected short secondi;
	 protected double bolletta;
	 

	public TelefonoFisso(short ore, short minuti, short secondi) {
		this.ore = ore;
		this.minuti = minuti;
		this.secondi = secondi;
	}
	public void setBolletta() {
		//12 cent al minuto:
		//bolletta in euro  
		bolletta = (ore * 720 + minuti * 12 + secondi * 0.2)/100; 
	}	 
	public double getBolletta() {
		return bolletta;
	}
	
	public short getOre() {
		return ore;
	}
	public void setOre(short ore) {
		this.ore = ore;
	}
	public short getMinuti() {
		return minuti;
	}
	public void setMinuti(short minuti) {
		this.minuti = minuti;
	}
	public short getSecondi() {
		return secondi;
	}
	public void setSecondi(short secondi) {
		this.secondi = secondi;
	}
		 
}
