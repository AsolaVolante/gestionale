package util;

public interface Comparable {
	
	int compareTo(Comparable o);

}
