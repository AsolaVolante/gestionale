package esercizi;

import java.awt.Container;

import javax.swing.JFrame;

public class DemoCircleBorder extends JFrame{

	public DemoCircleBorder() {
		super("DemoCircleBorder");
		Container c = getContentPane();
		CircleBorder panel = new CircleBorder();
		c.add(panel);
		setVisible(true);
		setSize(1000, 1000);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {		
		DemoCircleBorder dcb = new DemoCircleBorder();
	}
}
