package esercizi;

import util.Array;
import util.Comparable;

public class DemoArrayQuadri {

	public static void main(String[] args) {

		util.Comparable q1 = new Quadri("Rocco Siffredi", "Yolo", 300, 400);
		util.Comparable q2 = new Quadri("Condom", "Rocco Siffredi non è un artista", 200,200);
		util.Comparable q3 = new Quadri("Van Fog", "melo", 100, 100);
		util.Comparable q4 = new Quadri("Ciccio Gamer merda", "scroto peloso", 400, 400);
		
		util.Comparable[] galleria={q1,q2,q3,q4};
		
		Array.sort(galleria);
		
		for(int i=0; i<galleria.length; i++)
			System.out.println(galleria[i].toString());		
	}
}
