package esercizi;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;

public class VerticiLayoutDemo extends JFrame{

	Container c = getContentPane(); 
	JButton b = new JButton("b");
	JButton b1 = new JButton("b1");
	JButton b2 = new JButton("b2");
	JButton b3 = new JButton("b3");
	
	public VerticiLayoutDemo() {
		super("VerticiLayoutDemo");
		c.add(b);
		c.add(b1);
		c.add(b2);
		c.add(b3);
		c.setLayout(new VerticiLayout());
		setVisible(true);
		setSize(700,700);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		VerticiLayoutDemo vld = new VerticiLayoutDemo();
	}

}
