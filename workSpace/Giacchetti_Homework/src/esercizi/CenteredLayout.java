package esercizi;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

public class CenteredLayout implements LayoutManager {

	Component[] c;
	
	@Override
	public void addLayoutComponent(String arg0, Component arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void layoutContainer(Container parent) {
		int x=parent.getWidth(),y=parent.getHeight();
		c = parent.getComponents();
		
		if(x<500 || y<500)
			c[c.length-1].setVisible(false);
		else{
			c[c.length-1].setBounds(((x-(x/10))/2),((y-(y/10))/2), x/10, y/10);
			c[c.length-1].setVisible(true);
		}
	}

	@Override
	public Dimension minimumLayoutSize(Container arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dimension preferredLayoutSize(Container arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeLayoutComponent(Component arg0) {
		// TODO Auto-generated method stub

	}

}
