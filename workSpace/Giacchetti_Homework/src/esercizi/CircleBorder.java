package esercizi;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class CircleBorder extends JPanel {

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int w=this.getWidth(), h=this.getHeight();
		int l=w/2, a=h/2;
		int x=(w-l)/2, y=(h-a)/2;
		int spess=l/8;
		g.setColor(Color.BLACK);
		g.fillOval(x, y, l, a);
		g.setColor(Color.RED);
		for(int i=0; i<spess; i++){
			g.drawOval(x+i, y+i, l, a);
			l-=2;
			a-=2;
		}
	}
}
