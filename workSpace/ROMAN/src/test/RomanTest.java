package test;

import static org.junit.Assert.*;

import org.junit.Test;

public class RomanTest {
	private void check(String expectedString, int decimal) {
		assertEquals(expectedString, new Roman(decimal).toString());
	}
	@Test
	public void zeroIsEmpty() {
		check("", 0);
	}
	@Test
	public void oneIsI() {
		check("I", 1);
	}
	@Test
	public void twoIsII() {
		check("II", 2);
	}
	@Test
	public void threeIsIII() {
		check("III", 3);
	}
	@Test
	public void foiurIsIV() {
		check("IV", 4);
	}
	@Test
	public void fiveIsV() {
		check("V", 5);
	}
	@Test
	public void sixIsVI() {
		check("VI", 6);
	}
	@Test
	public void nineIsIX() {
		check("IX", 9);
	}
	@Test
	public void tenIsX() {
		check("X", 10);
	}
	@Test
	public void fifteenIsXV() {
		check("XV", 15);
	}
	@Test
	public void FourteenIsXIV() {
		check("XIV", 14);
	}
	@Test
	public void fourtyIsXL() {
		check("XL", 40);
	}
	@Test
	public void fourtyfourIsXIVL() {
		check("XLV", 45);
	}
}
