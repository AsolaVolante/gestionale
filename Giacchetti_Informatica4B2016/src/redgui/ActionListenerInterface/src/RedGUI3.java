package redgui.ActionListenerInterface.src;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class RedGUI3 {
	
	private JFrame frame;
	
	class MyListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			frame.getContentPane().setBackground(Color.RED);
			
		}
		
	}

	public RedGUI3() {
		super();
		frame = new JFrame("project ActionListenerInterface");
		JButton button = new JButton("OK");
		frame.add(button, BorderLayout.SOUTH);
		button.addActionListener(new MyListener());
		frame.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new RedGUI3();
				
			}
		});
	}
}


