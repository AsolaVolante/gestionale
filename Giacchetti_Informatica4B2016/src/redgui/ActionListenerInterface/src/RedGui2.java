package redgui.ActionListenerInterface.src;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class RedGui2 implements ActionListener {
	
	private JFrame frame;
	

	public RedGui2() {
		super();
		frame = new JFrame("project ActionListenerInterface");
		JButton button = new JButton("OK");
		frame.add(button, BorderLayout.SOUTH);
		button.addActionListener(this);
		frame.setVisible(true);

	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new RedGui2();
				
			}
		});
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		
		frame.getContentPane().setBackground(Color.RED);
		
	}
}

