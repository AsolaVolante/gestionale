package jPanel_Varie_Interfaccie;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class Animazione extends JPanel {
	
	Image image []= new Image[14]; 
	int i = 1;
	
	public Animazione(Image image[]){
		
		this.image = image;
		
		Timer timer = new Timer(50, new ActionListener() {
	        
            public void actionPerformed(ActionEvent e) {
            	if(i == 13) 
    	        	i = 0;
            	repaint();
                i++;
            }
            
        });
        
        timer.setRepeats(true);
        timer.start();

	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(image[i], 50, 50, null);
	}
}