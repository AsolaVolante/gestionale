package jPanel_Varie_Interfaccie;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class GridLayoutDemo {

	public static void main(String[] args) {
		GridLayout gridLayout =  new GridLayout(3, 4, 10, 10);
		JFrame j = new JFrame("Test");
		JLabel la = new JLabel("Label");
		JButton b = new JButton("Button");
		JButton b2 = new JButton("Button 2");
		JButton b3 = new JButton("Button 3");
		JButton b4 = new JButton("Button 4");
		j.setLayout(gridLayout);
		j.add(la);
		j.add(b);
		j.add(b2);
		j.add(b3);
		j.add(b4);
		j.setVisible(true);
		j.setSize(200,200);
	}


}
