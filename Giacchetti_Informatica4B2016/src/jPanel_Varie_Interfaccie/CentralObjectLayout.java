package jPanel_Varie_Interfaccie;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

public class CentralObjectLayout implements LayoutManager {

	private Component[] components;

	@Override
	public void addLayoutComponent(String name, Component comp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeLayoutComponent(Component comp) {
		// TODO Auto-generated method stub

	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void layoutContainer(Container parent) {
		// TODO Auto-generated method stub
		System.out.println("layoutContainer");
		components = parent.getComponents();
		
		int i = components.length-1;
		
		int width = (int) parent.getWidth()/10;
		int height =(int) parent.getHeight()/10;
		if(height<=50)
			height=50;
		if(width<=50)
			width=50;
		int x = (int) (parent.getWidth()/2)-(width/2);
		int y =(int) (parent.getHeight()/2)-(height/2);
		
		components[i].setBounds(x, y, width, height);
		

	}

}
