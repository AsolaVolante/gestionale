package jPanel_Varie_Interfaccie;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class BottoneCentralizzato {

	public static void main(String[] args) {
		JFrame f=new JFrame("Centro");
		//f.getContentPane();
		
		JButton b1= new JButton("Center");
		
		JPanel p=new JPanel();
		
		f.getContentPane().add(p);
		
		p.add(b1);
		
		p.setLayout(new CentralObjectLayout());
		
		p.setBackground(Color.RED);
		f.setVisible(true);
		f.setBounds(100,100,100,100);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}
