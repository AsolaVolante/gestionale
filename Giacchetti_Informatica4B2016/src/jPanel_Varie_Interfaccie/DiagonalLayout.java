package jPanel_Varie_Interfaccie;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

public class DiagonalLayout implements LayoutManager {

	private Component[] components;

	@Override
	public void addLayoutComponent(String name, Component comp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeLayoutComponent(Component comp) {
		// TODO Auto-generated method stub

	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void layoutContainer(Container parent) {
		// TODO Auto-generated method stub
		System.out.println("layoutContainer");
		
		components = parent.getComponents();
		
		for (int i = 0; i < components.length; i++) {
			int width = (int) parent.getWidth()*((1/3)*i+1);
			int x = (int) parent.getWidth()*((1/3)*i+1)*i;
			int y =(int) parent.getHeight()*((1/3)*i+1)*i;
			int height =(int) parent.getHeight()*((1/3)*i+1);
			components[i].setBounds(x, y, width, height);
		}

	}
}