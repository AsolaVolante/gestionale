package scuolaGui;

public class SalvaDaTastiPanel {

	private String a, b, c;
	
	public SalvaDaTastiPanel(String a, String b, String c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public void setA(String a) {
		this.a = a;
	}

	public void setB(String b) {
		this.b = b;
	}

	public void setC(String c) {
		this.c = c;
	}
	
}
